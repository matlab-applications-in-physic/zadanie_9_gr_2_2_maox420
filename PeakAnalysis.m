%Matlab 2011a script
%
%author: Mateusz Ni�nik
%date: 10-01-2019

clear;
%cd 'C:\Users\student\Desktop';
cd 'D:\';

fileID=fopen('z3_1350V_x20.dat','r');   %I'm using file downloaded from https://drive.google.com/file/d/1ev1PCpxKYg47ohglfXdqzW-oROtwnllu/view?usp=sharing
fileInfo=dir('z3_1350V_x20.dat');
fileSize = fileInfo.bytes;

%MatlabInfo = memory;
%fraction = MatlabInfo.MaxPossibleArrayBytes / 40;

value = zeros(255, 1);

fraction=50e6;

for j=1:fix(fileSize/fraction)              %Reading data usually takes about 10 minutes (on unieveristy computer)
                                            %to track how long does it take program displays variable j, which goes from 1 to 171
    data=fread(fileID,fraction,'uint8');
    
    for k=1:fraction
         value(data(k)) = value(data(k)) + 1;
    end
    disp (j); 
end           

fclose(fileID);

x=(1:255);

bar(x, value);
title('Histogram');
ylabel('Number of occurences');
xlabel('Values');

%saveas(gcf, 'z3_1350V_x20_histogram.pdf');

%figure(1);
%P = plot(data);
